const axios = require('../confg/api');
var fs = require('fs');

const readFile = require('./util/readFile');

module.exports = {
    async createMeeting(req, res) {
        const { zoom_token } = req.headers;
        const { 
            userId, // String -> user id or emaill
            type, // Integer -> 1: Instant meeting | 2: Schedule meeting | 3: Recurring with no fixed time | 4: Recuring meeting with fixed time
            schedule_for, // String -> If you woud like to schedule this meeting for someone else account, provide Zoom user id or email of the user
            timezone, // String -> Meeting time zone | https://marketplace.zoom.us/docs/api-reference/other-references/abbreviation-lists#timezones
            // settings begin
            host_video, // Boolean -> Start video when the host join the meeting
            participant_video, // Boolean -> Start video when participants join the meeting
            cn_meeting, // Boolean -> Host meeting in China
            in_meeting, // Boolean -> Host meeting in India
            join_before_host, // Boolean -> Allow participants to join the meeting before the host starts the meeting
            jbh_time, // Integer -> 0: ALlow participant to join anytime | 5: Allow particicpant to join 5 minutes before meeting start time | 10: Allow participant to join 10 minutes before meeting start time
            mute_upon_entry, // Boolean -> Mute participants upon entry
            watermark, // Boolean -> Add watermark when viewing a shared screen
            use_pmi, // Boolean -> User personal ID instead of an automatically generated meeting ID
            approval_type, // Integer -> 0: Automatically approve | 1: Manually approve | 2: No registration required
            registration_type, // Integer -> 1 Attendees register once and can attend any time of occurrences | 2: Attendees neet to register for each occurrence to attend | 3: Attendees register once and can choose one or more occurences to attend
            audio, // String -> both: Both Teleohony and VoiP | Telephony: Telephony only | voip: VoiP only
            auto_recording, // String -> local: Record on local | cloud: Record on cloud | none: Disable
            alternative_host, // String -> Alterbatuve host's emails or IDs: multiple values separated by a comma
            close_registration, // Boolean -> Clore registration after event date
            wating_room, // Boolean -> Enable waiting room.
            global_dial_in_countries, // array[String] -> List of global dial-in countries
            contact_name,  // String -> Contact name
            contact_email, // String -> Contact email
            registrants_email_notification, // Boolean -> Send email notifications to registrants about approval, cancellation, denial of the registraton
            meeting_authentication, // Boolean -> Only authentication users can join the meeting https://support.zoom.us/hc/en-us/articles/360037117472-Authentication-Profiles-for-Meetings-and-Webinars
            authentication_option, // String -> Specify authentication options https://marketplace.zoom.us/docs/api-reference/zoom-api/users/usersettings 
            authentication_domains, // String -> Allow to specify he rule so that Zoom users
            addtional_data_center_regions, // array[String] -> allow additional data center regions for the meeting  https://support.zoom.us/hc/en-us/articles/360042411451-Selecting-data-center-regions-for-hosted-meetings-and-webinars 
            language_interpretation, // object{ Boolean -> enable, array[object{ String -> email address of the interpreter, String -> languages for the interpreter }] }
            show_share_button, // Boolean -> The registration page for the meeting will include social share buttons
            allow_multiple_devices, // Boolean -> Allow to join a meeting from multiple devices
            encryption_type, // String -> enhaced_encryption: Encryption is stored in the cloud | e2ee: End-toend encryption
            approved_or_denied_countries_or_regions,
            // settings end
        } = req.body;
        
        const file = req.file;
       
        try {   
            const filePath = file.destination + '\\' + file.filename;
            const fileData = await readFile.readFile(filePath);

            const config = { 
                headers: {  
                    Authorization: `Bearer ${zoom_token}` 
                } 
            };
            var success = 0;
            var failed = 0; 

            const newData = await fileData.map(async (meetingConfig) => {

                if(meetingConfig.errors.length > 0) {
                    failed++;
                    return meetingConfig;
                }

                const data = {
                    "topic": meetingConfig.topic, 
                    "start_time": meetingConfig.start_time,
                    "duration": meetingConfig.duration,
                    "password": meetingConfig.password,
                    "agenda": meetingConfig.agenda,
                    "type": type || 2,
                    "timezone": timezone,
                    "schedule_for": schedule_for,
                    "settings": {
                        "host_video": host_video || false,
                        "participant_video": participant_video || false,
                        "cn_meeting": cn_meeting || false,
                        "in_meeting": in_meeting || false,
                        "join_before_host": join_before_host || false,
                        "jbh_time": jbh_time || 0,
                        "mute_upon_entry": mute_upon_entry || true,
                        "watermark": watermark || false,
                        "use_pmi": use_pmi || false,
                        "approval_type": approval_type || 0,
                        "registration_type": registration_type || 1,
                        "audio": audio || "both",
                        "auto_recording": auto_recording || "none",
                        "alternative_host": alternative_host,
                        "close_registration": close_registration || false,
                        "wating_room": wating_room || false,
                        "global_dial_in_countries": global_dial_in_countries,
                        "contact_name": contact_name,
                        "contact_email": contact_email,
                        "registrants_email_notification": registrants_email_notification || true, 
                        "meeting_authentication": meeting_authentication || false,
                        "authentication_option": authentication_option,
                        "authentication_domains": authentication_domains,
                        "addtional_data_center_regions": addtional_data_center_regions,
                        "language_interpretation": language_interpretation,
                        "show_share_button": show_share_button || false,
                        "allow_multiple_devices": allow_multiple_devices || true,
                        "encryption_type": encryption_type, 
                        "approved_or_denied_countries_or_regions": approved_or_denied_countries_or_regions 
                    }
                };

                await axios.post(`/users/${userId}/meetings`, data, config)
                .then(async (response) => { 
                    meetingConfig.meeting = response.data;
                    console.log(response.data)
                    success++;
                })
                .catch(async (error) => { 
                    meetingConfig.meeting = error.response.data;
                    failed++
                });

                return meetingConfig;
            });

            console.log(newData)

            fs.unlinkSync(filePath);
            return res.send({ success, failed, newData }); 
        }
        catch(error){ 
            console.log(error)
            return res.status(500).send({ ok: false, message: 'Error', log: error });
        }
    },
    
    async list (req, res) {
        const { type, page_size, next_page_token = '', page_number, user_id } = req.body;
        const { zoom_token } = req.headers;

        try {
            config = { 
                headers: {  
                    Authorization: `Bearer ${zoom_token}`
                } 
            };

            await axios.get(`/users/${user_id}/meetings/?type=${type}&page_size=${page_size}&page_number=${page_number}&page_token=${next_page_token}`, config)
            .then((response) => {
                return res.send({ ok: true, meetings: response.data.meetings });
            })
            .catch((error) => {
                return res.status(error.response.status).send({ ok: false, error: { message: error.response.data.message, code: error.response.data.code } });
            });
        }
        catch (error) {
            console.log(error)
            return res.status(500).send({ ok: false, message: 'Error', log: error });
        }
    }
}