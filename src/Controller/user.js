const axios = require('../confg/api');

module.exports = {
    async get_user_by_email(req, res) {
        const { email, login_type } = req.body;
        const { zoom_token } = req.headers

        if(email == null || login_type == null || zoom_token == null)
            return res.status(422).send({ ok: false, error: { code: 422 , message: 'Missing params'} } );

        try {
            config = { 
                headers: {  
                    Authorization: `Bearer ${zoom_token}` 
                } 
            };

            axios.get(`/users/${email}?login_type=${login_type}`, config)
            .then((response) => {
                return res.send({ ok: true, user: response.data });
            })
            .catch((error) => {
                return res.status(error.response.data.code).send({ ok: false, error: { message: error.response.data.message, code: error.response.data.code } });
            });
    
        }
        catch(error) {
            console.log(error)
            return res.status(500).send({ ok: false, message: 'Error', log: error });
        }
    },

    async get_user_by_id(req, res) {
        const { user_id, login_type } = req.body;
        const { zoom_token } = req.headers

        if(email == null || login_type == null || zoom_token == null)
            return res.status(422).send({ ok: false, error: { code: 422 , message: 'Missing params'} } );

        try {
            config = { 
                headers: {  
                    Authorization: `Bearer ${zoom_token}` 
                } 
            };

            await axios.get(`/users/${user_id}?login_type=${login_type}`, config)
            .then((response) => {
                return res.send({ ok: true, user: response.data });
            })
            .catch((error) => {
                return res.status(error.response.status).send({ ok: false, error: { message: error.response.data.message, code: error.response.data.code } });
            });
    
        }
        catch(error) {
            console.log(error)
            return res.status(500).send({ ok: false, message: 'Error', log: error });
        }
    },
};