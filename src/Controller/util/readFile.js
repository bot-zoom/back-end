const xlsx = require('xlsx');

module.exports = {
    async readFile(filePath) {
        const wb = xlsx.readFile(filePath, { cellDates: true }); // read file and save in wb
    
        if(wb.SheetNames.length != 1)
            return { ok: false, error: 'Table must have only one sheet' };
    
        ws = wb.Sheets[wb.SheetNames[0]]
    
        data = xlsx.utils.sheet_to_json(ws);
    
        const newData = data.map((meetingConfig) => {
            meetingConfig.errors = [];
    
            if(meetingConfig.start_time == null)
                meetingConfig.errors.push('Missing start_time');
            if(meetingConfig.duration == null)
                meetingConfig.errors.push('Missing duration');
            if(meetingConfig.topic == null)
                meetingConfig.errors.push('Missing topic');
            
            if(typeof(meetingConfig.duration) !== 'number')
                meetingConfig.errors.push('Duration must be a number');
            if(meetingConfig.topic != null && typeof(meetingConfig.topic) !== 'string')
                meetingConfig.errors.push('Topic must be a string');
            if(meetingConfig.agenda != null && typeof(meetingConfig.agenda) !== 'string')
                meetingConfig.errors.push('Agenda must be a string');
            
            if(isNaN(new Date(meetingConfig.start_time)))
                meetingConfig.errors.push('Invalid format must be in format dd/mm/yyyy hh:mm');

            if(typeof(meetingConfig.duration) === 'number' && meetingConfig.duration < 1)
                meetingConfig.errors.push('Duration must be a positive number');
    
            return meetingConfig;
        });
    
        return newData;
    }
}