const router = require('express').Router();
const meetingController = require('../Controller/meeting');
const multer = require('multer');

router.post('/create', multer(require('../confg/multer')).single('file'), meetingController.createMeeting);
router.get('/list', meetingController.list);

module.exports = router;