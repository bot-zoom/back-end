const router = require('express').Router();
const userController = require('../Controller/user');

router.get('/getUserByEmail', userController.get_user_by_email);
router.get('/getUserById', userController.get_user_by_id);

module.exports = router;