const multer = require('multer');
const crypto = require('crypto');
const path = require('path');

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, path.resolve(__dirname, '..', '..' , 'tmp', 'uploads'));
    },
    filename: function(req, file, cb) {
        crypto.randomBytes(16, async (err, hash) => {
            cb(null, `${hash.toString('hex')}.xlsx`);
        });
    }
});

module.exports = {
    dest: path.resolve(__dirname, '..', '..' , 'tmp', 'uploads'),
    storage: storage,
    limits: {
        fileSize: 100 * 1024 * 1024,
    },
    fileFilter: (req, file, cb) => {
        // const alloedMimes = [
        //     'image/jpeg',
        //     'image/jpg',
        //     'image/png',
        //     'image/pjpeg',
        // ];

        // if(alloedMimes.includes(file.mimetype))
            cb(null, true);
        // else
        //     cb(new Error('Invalid file type'));
    }
};