require("dotenv").config({
    path: process.env.NODE_ENV == 'test' ? '.env.test' : '.env'
});

const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const user = require("./Controller/user");

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan('dev'));
app.use(cors());
app.use((request, response, next) => { //middleware
    response.header("Access-Control-Allow-Origin", "*");
    response.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    next();
});


const meetingRoute = require('./Routes/meeting');
const userRoute = require('./Routes/user');

app.use('/meeting', meetingRoute);
app.use('/users', userRoute);


const port = process.env.PORT || 3333;
module.exports = app.listen(port, () => console.log(`app is listening on port ${port}`));